import os

import pandas as pd

from gdrive.process_image import ProcessImage

data_folder = '/home/violetta/Documents/work/МАКЕТЫ_test/'


class Test:
    def __init__(self):
        pass

    def test_image(self):
        result_df = pd.DataFrame(columns=['image_name', 'prediction', 'true'])
        for folder_name in os.listdir(data_folder):
            if os.path.isdir(data_folder + folder_name):
                class_path = data_folder + folder_name
                if folder_name != '[]' and os.path.isdir(class_path):
                    for file_name in os.listdir(class_path + '/1x/'):
                        try:
                            if file_name == '[]':
                                for img in os.listdir(class_path + '/1x/[]/'):
                                    try:
                                        result_df = self.get_prediction(class_path, '[]/' + img, '[]', result_df)
                                    except Exception as e:
                                        print(e, img)
                            else:
                                result_df = self.get_prediction(class_path, file_name, folder_name, result_df)
                        except Exception as e:
                            print(e, class_path + '/1x/' + file_name)
        result_df = self.check_results(result_df)
        result_df.to_csv(data_folder + 'results.csv', index=False)

    def get_prediction(self, class_path, file_name, folder_name, result_df):
        result = ProcessImage().process_image(class_path + '/1x/', file_name)
        result_df = self.parse_results(result, class_path + '/1x/' + file_name, result_df,
                                       folder_name)
        return result_df

    def parse_results(self, result_dict, image_name, result_df, folder_name):
        for key in result_dict.keys():
            if not result_dict[key]:
                result_dict[key] = '[]'
            row = [image_name, result_dict[key], folder_name]
            print(row)
            result_df.loc[len(result_df)] = row
        return result_df

    def check_results(self, result_df):
        check_list = []
        for i in range(len(result_df)):
            if result_df.loc[i]['true'] in result_df.loc[i]['prediction'] or result_df.loc[i]['true'] == \
                    result_df.loc[i]['prediction']:
                check_list.append(1)
            else:
                check_list.append(0)
        result_df['check'] = check_list
        return result_df


if __name__ == "__main__":
    Test().test_image()
