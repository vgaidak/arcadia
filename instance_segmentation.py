import torch
import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor

from consts import DEVICE, CLASSES, RELEVANT_THRESH_LEVEL
from utils import load_model_weights


class InstanceSegmentation:

    def __init__(self):
        pass

    def __get_model_instance_segmentation(self, num_classes):

        model = torchvision.models.detection.maskrcnn_resnet50_fpn(pretrained=True)

        in_features = model.roi_heads.box_predictor.cls_score.in_features

        model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

        in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels

        hidden_layer = 256

        model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask,
                                                           hidden_layer,
                                                           num_classes)

        return model

    def prepare(self, model_weights_path, device=None, num_classes=None):

        if not num_classes:
            num_classes = len(CLASSES) + 1

        if not device:
            device = DEVICE

        if not model_weights_path:
            raise FileNotFoundError('Failed to load instance segmentation model weights [%s]' % model_weights_path)

        model = self.__get_model_instance_segmentation(num_classes)
        model = load_model_weights(model=model, file_path=model_weights_path)
        model.to(device)

        return model

    # not used
    def detect_objects(self, img, model, device):

        with torch.no_grad():
            prediction = model([img.to(device)])[0]

        relevant_probs = [x for x in prediction['scores'].cpu().detach().tolist() if x >= RELEVANT_THRESH_LEVEL]

        for key in prediction:
            prediction[key] = prediction[key][:len(relevant_probs)]

        pred_masks = prediction['masks'].cpu().detach().numpy()
        pred_labels = prediction['labels'].cpu().detach().numpy()
        pred_boxes = prediction['boxes'].cpu().detach().numpy()
        pred_scores = prediction['scores'].cpu().detach().numpy()

        objs = []
        for i, b in enumerate(pred_boxes):
            label_id = int(pred_labels[i])
            score = round(float(pred_scores[i]), 2)
            b = tuple([int(a) for a in b.tolist()])
            # single [obj] structure
            # (object label id (check consts classes def), prediction score, box coords, mask)
            objs.append((label_id, score, (b[0], b[1], b[2], b[3]), pred_masks[i]))

        return objs
